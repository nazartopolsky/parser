class Product:
    id = None
    title = None
    sku_number = None
    url = None
    image_url = None
    buy_url = None
    short_description = None
    description = None
    discount = None
    discount_type = None
    currency = None
    retail_price = None
    sale_price = None
    brand = None
    manufacturer = None
    shipping_charge = None
    availability = None
    sizes = None
    materials = None
    colors = None
    style = None
    gender_group = None
    age_group = None

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def to_dict(self, skip_none=False):
        variables = [
            x for x in self.__class__.__dict__
            if not (x.startswith('__') or callable(getattr(self, x)))
        ]
        dict_ = {}
        for var in variables:
            value = getattr(self, var, None)
            if not skip_none or value is not None:
                dict_[var] = value
        return dict_
