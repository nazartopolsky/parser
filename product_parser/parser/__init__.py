from importlib import util as _util
from ..db import get_default_storage

if _util.find_spec('lxml'):
    from .xml import XMLParser

    def parse_xml(file, storage=None):
        if storage is None:
            storage = get_default_storage()
        if isinstance(file, str):
            file = open(file, mode='rb')
        parser = XMLParser(file)
        with storage:
            for obj in parser.parse():
                storage.add(obj)

from .csv import CSVParser

def parse_csv(file, storage=None):
    if storage is None:
        storage = get_default_storage()
    if isinstance(file, str):
        file = open(file)
    parser = CSVParser(file)
    with storage:
        for obj in parser.parse():
            storage.add(obj)
