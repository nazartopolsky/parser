from lxml import etree
from ..models import Product
from .base import ObjectParser, Parser


class XMLObjectParser(ObjectParser):
    """
    Convert an XML node to an object.
    By default, will only retrieve data from mapping.
    """
    model = Product
    add_unknown = False
    data_node_tag = 'item_basic_data'
    mapping = {
        'item_unique_id': 'id',
        'item_title': 'title',
        'item_long_desc': 'description',
        'item_page_url': 'url',
        'item_image_url': 'image_url',
        'amzn_page_url': 'buy_url',
        'item_price': 'retail_price',
        'item_sku': 'sku_number',
        'item_shipping_charge': 'shipping_charge',
    }

    def parse(self, item_node):
        model = self.model()
        data_node = item_node.find(self.data_node_tag)
        if data_node is not None:
            for node in data_node.iterchildren('*'):
                if node.tag in self.mapping:
                    self.add_from_mapping(model, node)
                else:
                    self.add_unknown_element(model, node)
        return model

    def add_from_mapping(self, obj, node):
        attr_name = self.mapping[node.tag]
        setattr(obj, attr_name, node.text)

    def add_unknown_element(self, obj, node):
        if self.add_unknown:
            setattr(obj, node.tag, node.text)


class XMLParser(Parser):
    object_parser = XMLObjectParser
    tag_name = 'item_data'

    def __init__(self, stream):
        super(XMLParser, self).__init__(stream)

    def parse(self):
        object_parser = self.object_parser()
        context = iter(etree.iterparse(self.stream, events=('start', 'end')))
        event, root = next(context)
        for event, element in context:
            if event == "end" and element.tag == self.tag_name:
                yield object_parser.parse(element)
                root.clear()
