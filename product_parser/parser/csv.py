import csv as _csv
from .base import ObjectParser, Parser
from ..models import Product


class CSVLineParser(ObjectParser):
    model = Product

    skip = '_'
    fields = [
        'id',
        'title',
        'sku_number',
        'brand',
        skip,
        'url',
        'image_url',
        skip,
        'short_description',
        'description',
        skip,
        'discount_type',
        'retail_price',
        'sale_price',
        skip,
        skip,
        'manufacturer',
    ]

    def parse(self, line):
        kwargs = {x[0]: x[1] for x in zip(self.fields, line)
                  if x[0] != self.skip}
        return self.model(**kwargs)


class CSVParser(Parser):
    object_parser = CSVLineParser
    separator = '|'

    def __init__(self, stream):
        super(CSVParser, self).__init__(stream)
        self.reader = _csv.reader(self.stream, delimiter=self.separator)

    def parse(self):
        object_parser = self.object_parser()
        for line in self.reader:
            yield object_parser.parse(line)
