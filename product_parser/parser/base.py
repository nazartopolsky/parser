import abc


class ObjectParser(abc.ABC):
    """Convert a source from a stream to an object"""
    @abc.abstractmethod
    def parse(self, sourse):
        pass


class Parser(abc.ABC):
    """Parse a stream and yield objects, one by one."""

    object_parser = None

    def __init__(self, stream):
        self.stream = stream

    @abc.abstractmethod
    def parse(self):
        pass

    def get_object_parser(self):
        return self.object_parser()
