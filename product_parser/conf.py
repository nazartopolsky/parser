import logging


class Config:
    mongodb_uri = ''
    mongodb_database_name = ''
    mongodb_max_storage_size = 300

    logging = {
        'format': '[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s',
        'level': logging.INFO,
        'filename': '../log/parser.log',
    }


config = Config()


def configure(**kwargs):
    for key, value in kwargs.items():
        if hasattr(config, key):
            setattr(config, key, value)
        else:
            raise AttributeError('Unknown setting: {}'.format(key))
    logging.basicConfig(**config.logging)
