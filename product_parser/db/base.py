import abc
import re
from collections import defaultdict


class Storage(abc.ABC):
    cache = None
    pluralize_names = True

    def __init__(self):
        self.cache = self.new_cache()

    def add(self, obj):
        self.add_to_cache(obj)

    @abc.abstractmethod
    def save(self):
        pass

    def add_to_cache(self, obj):
        table = self.table_name(obj)
        self.cache[table].append(obj)

    def new_cache(self):
        return defaultdict(list)

    def table_name(self, obj):
        name = _to_snake_case(obj.__class__.__name__)
        if self.pluralize_names:
            name += 's'
        return name


def _to_snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
