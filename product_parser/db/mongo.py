import pymongo
from ..conf import config
from .base import Storage
import logging


def get_default_storage():
    return MongoStorage(get_default_client())


def get_default_client():
    return pymongo.MongoClient(config.mongodb_uri)


class MongoStorage(Storage):
    def __init__(self, client=None):
        super(MongoStorage, self).__init__()
        if client is None:
            client = get_default_client()
        self.client = client
        self.db_name = config.mongodb_database_name
        self.max_storage_size = config.mongodb_max_storage_size

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.save()

    def add(self, obj):
        super(MongoStorage, self).add(obj)
        self.save_if_should()

    def check_duplicates(self, table_name, *fields):
        collection = self.db[table_name]
        duplicates = {}
        for field in fields:
            group_by = '$' + field
            duplicates[field] = collection.aggregate([
                 {'$group' : {'_id': group_by, 'count': { '$sum': 1 } } },
                 {'$match': {'_id' :{ '$ne' : None } , 'count' : {'$gt': 1} } }, 
                 {'$project': {field : '$_id', '_id' : 0} },
            ])
        return duplicates

    @property
    def db(self):
        return self.client[self.db_name]

    def save_if_should(self):
        if self._count_objects() > self.max_storage_size:
            self.save()

    def save(self):
        cache = self.cache
        self.cache = self.new_cache()
        for table_name, objects in cache.items():
            self.save_table(table_name, objects)

    def save_table(self, name, objects):
        if objects:
            self.db[name].ensure_index('id')
            existing_ids = self._existing_ids(name, objects)
            operations = []
            for obj in objects:
                if obj.id in existing_ids:
                    operations.append(self._update(obj))
                else:
                    operations.append(self._insert(obj))
            result = self.db[name].bulk_write(operations)
            self.logger.info(
                'Saving objects to {table}: {inserted} inserted, {modified} modified.'.format(
                    table=name,
                    inserted=result.inserted_count,
                    modified=result.modified_count,
                )
            )

    def _count_objects(self):
        total = 0
        for objects in self.cache.values():
            if objects is not None:
                total += len(objects)
        return total

    def _existing_ids(self, table, objects):
        ids = {x.id for x in objects}
        query = {'id': {'$in': list(ids)}}
        return set(self.db[table].find(query).distinct('id'))

    def _insert(self, obj):
        return pymongo.InsertOne(obj.to_dict())

    def _update(self, obj):
        data = obj.to_dict()
        data.pop('id', None)
        return pymongo.UpdateOne({'id': obj.id}, {'$set': data}, upsert=False)

    @property
    def logger(self):
        return logging.getLogger(__name__)
