from importlib import util as _util

if _util.find_spec('pymongo'):
    from .mongo import MongoStorage, get_default_client, get_default_storage
