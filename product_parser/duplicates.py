from .db import get_default_storage


def find_duplicate_products(storage=None, fields=('name',)):
    """Finds duplicates by given fields"""
    if storage is None:
        storage = get_default_storage()
    return storage.check_duplicates('products', *fields)
