Allows parsing csv and xml files and storing the results to MongoDB.

Example usage can be found in `example.py`.