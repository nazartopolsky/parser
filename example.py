from product_parser import configure, get_default_storage, parse_csv, parse_xml, find_duplicate_products

configure(mongodb_uri='mongodb://localhost:27017/', mongodb_database_name='products')

storage = get_default_storage()

parse_csv('products.txt', storage)
parse_xml('products.xml', storage)

duplicates = find_duplicate_products()
